#include <Arduino.h>//Default csomag a platformio-hoz
#include <WiFi.h>             //Na mi lehet ez ??
#include <HTTPClient.h>       //Izé az MQTT-hez
#include <ESP32httpUpdate.h>  //Firmware frissítésért felel
#include <config.h>           //Konfig file
#include <PubSubClient.h>     //Das ist MQTT Aufschläger
#include <Crypto.h>           //Márk paranoid dolga
#include <ArduinoJson.h>      //A szeretett Json package
#include <WiFiUdp.h>          //NTP hez socket
#include <NTPClient.h>        //Tárcsázd fel:180 (működik)

bool update = false;
bool debug = true;//A szent debug parancs, production-be ne kapcsold be!!!!

WiFiClient espclient;
PubSubClient client(espclient);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "0.hu.pool.ntp.org", 7200, 60000);//NTP szerver és offest init

long lastMsg = 0;
int value = 0;


//Beépített hőmérséklet érzékelő
#ifdef __cplusplus
extern "C" {
#endif
uint8_t temprature_sens_read();
#ifdef __cplusplus
}
#endif
uint8_t temprature_sens_read();
//Beépített hőmérséklet érzékelő


void setup_wifi()
{
  //Ha nem találtad ki, akkor ez felel a wifi kapcsolódásért
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }
}


String isSHAHash(String text)
{       //Ezzel nem kell törődni, Márk megoldja!!!!
        char needToHash[100];
        text.toCharArray(needToHash, 100);
        SHA256 hasher;
        hasher.doUpdate(needToHash, strlen(needToHash));
        byte hash[SHA256_SIZE];
        hasher.doFinal(hash);

        String data = "";
        for (byte i=0; i < SHA256_SIZE; i++)
        {
                if (hash[i]<0x10) data += "0";
                data += String(hash[i], HEX);
        }
        return data;
}

void callback(char* topic, byte* payload, unsigned int length)
{
  timeClient.update();//Lekéri az NTP szerverről a pontos időt
  String now_time = String(timeClient.getFormattedTime());//Változóba tárolás

  String channel = String(topic);//Topik áltozóba tárolása
  if (String(IN_SUB).equals(channel))//Csak a szerver külön csatornájáról érkező üzeneteket fogadja el és dolgozza fel
  {
      String message = "";
      for (int i = 0; i < length; i++)
      {
        message += String(char(payload[i])); //byte* ról konverteli át string-re
      }


      DynamicJsonBuffer jsonInboxBuffer;
      JsonObject& getMessage = jsonInboxBuffer.parseObject(message);
      if (getMessage.success())
      {//getMessage-t utána lehet parsolni
        if (debug == true)
        {
            Serial.println("[M] Message recevied: ");
            getMessage.prettyPrintTo(Serial);
        }

        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.createObject();










        JsonObject& about = root.createNestedObject("about");
        about["node"] = String(NODE);
        about["type"] = NODE_TYPE;

        JsonObject& about_network = about.createNestedObject("network");
        about_network["ssid"] = WiFi.SSID();
        about_network["ip"] = WiFi.localIP().toString();
        about_network["gateway"] = WiFi.gatewayIP().toString();

        JsonObject& about_sensors = about.createNestedObject("sensors");
        about_sensors["temperature"] = ((temprature_sens_read() - 32) / 1.8);
        about_sensors["hall"] = hallRead();

        JsonObject& request = root.createNestedObject("request");
        request["card-id"] = "213412";
        request["hash"] = "2CF24DBA5FB0A30E26E83B2AC5B9E29E1B161E5C1FA7425E73043362938B9824";

        JsonObject& request_diagnostic = request.createNestedObject("diagnostic");
        request_diagnostic["icmp"] = "randomgeneraltadat";
        request_diagnostic["TTL"] = "255";

        JsonObject& response = root.createNestedObject("response");
        response["card-id"] = "213412";
        response["hash"] = "2CF24DBA5FB0A30E26E83B2AC5B9E29E1B161E5C1FA7425E73043362938B9824";

        JsonObject& response_diagnostic = response.createNestedObject("diagnostic");
        response_diagnostic["icmp"] = "randomgeneraltadat";
        response_diagnostic["TTL"] = "255";

        //Ha már a tököd is tele lenne a json-nal, akkor ajánlom figyelmedbe https://arduinojson.org/v5/assistant/

        char dasIstAbsenderPrellbock[900]; // Nagyon unom a json bufferek elnevezését!
        root.printTo(dasIstAbsenderPrellbock, sizeof(dasIstAbsenderPrellbock));
        client.publish(OUT_SUB,dasIstAbsenderPrellbock);
      }



      if((WiFi.status() == WL_CONNECTED) && update)
      {   //Ez lenni majd a frimware update cucc, csak még nincsen kész a json szerkezet
          t_httpUpdate_return ret = ESPhttpUpdate.update("http://192.168.0.102/firmware.bin");

          switch(ret)
          {
              case HTTP_UPDATE_FAILED:
                  Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
                  break;

              case HTTP_UPDATE_NO_UPDATES:
                  Serial.println("HTTP_UPDATE_NO_UPDATES");
                  break;

              case HTTP_UPDATE_OK:
                  Serial.println("HTTP_UPDATE_OK");
                  break;
          }
      }

  }
}
void setup()
{
    if (debug)//Debug kapcsoló LEL
    {
      Serial.begin(115200);
      while (!Serial){}//Csak azért, ha megnyitod a konzolt, akkor lássad a gyönyörű d3bug felíratot XD
      delay(2000);
    }

    setup_wifi();

    client.setServer(MQTT_SERVER, MQTT_port); client.setCallback(callback); //MQTT init
    timeClient.begin();//NTP init

    //Nem unatkoztam, azért érzitek :-D
    //HACK: Késöbb jobb lenne, ha kejelzné az ESP. fügvénytár pl. SDK verzióját
    Serial.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    Serial.println("||                                                                 ||");
    Serial.println("||                         D3BUG_M0D3_3N4BL3D                      ||");
    Serial.println("||                                                                 ||");
    Serial.println("||                   [*] Serial console service active...          ||");
    Serial.println("||                   [*] SPPIFS config loaded...                   ||");
    Serial.println("||                   [*] MQTT Server config init..                 ||");
    Serial.println("||                   [*] WiFi connected...                         ||");
    Serial.println("||                   [*] NTP service running...                    ||");
    Serial.println("||                                                                 ||");
    Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    Serial.println("\n");
    Serial.println("Telemetry messages:");

}

void reconnect()
{ //Ha elvész a connect akkor ez helyre pofozza!!!
  while (!client.connected())
  {
    if (client.connect("ESP8266Client", MQTT_USER, MQTT_PASSWORD))
    {
      Serial.println("[*] MQTT Server connecting: Connected");
      client.publish(OUT_SUB, "NODE-1 connected");
      client.subscribe(OUT_SUB);
      client.subscribe(IN_SUB);
    }
    else
    {
      Serial.println("[!] Failed MQTT connection");
      Serial.println("[?] Try 5 seconds later");
      delay(5000);
    }
  }
}
void loop()
{
    if (!client.connected()) reconnect();
    client.loop();
}
