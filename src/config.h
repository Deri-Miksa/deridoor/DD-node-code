int NODE = 1;
String NODE_TYPE = "ESP32";


const char* WIFI_SSID = "";
const char* WIFI_PASSWORD = "";

const char* MQTT_SERVER = "";
int MQTT_port = 8883;
const char* OUT_SUB = "";
const char* IN_SUB = "";

const char* MQTT_USER = "";
const char* MQTT_PASSWORD = "";
